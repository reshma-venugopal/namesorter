﻿using System.Collections.Generic;

namespace NameSorter
{
    /// <summary>
    /// This interface is implemented by the NameSorter classes
    /// to achieve various sorting functionalities
    /// </summary>
    public interface INameSorter
    {
        string[] ReadNames(string filePathToRead);
        IEnumerable<string> SortNames(string[] namesToSort);
        string WriteNames(IEnumerable<string> sortedNames, string filePathToWrite);
    }
}
