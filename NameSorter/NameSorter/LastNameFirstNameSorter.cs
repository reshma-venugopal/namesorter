﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NameSorter
{
    /// <summary>
    /// Reads the file that contains the names to be sorted - "Lastname, Firstname"
    /// Sorts all the names in the text file, first by lastname, then by firstname
    /// Creates another text file and saves the sorted results to the new file 
    /// </summary>
    public class LastNameFirstNameSorter : INameSorter
    {
        /// <summary>
        /// Reads the contents of the unsorted file to an array if file path is valid
        /// </summary>
        /// <param name="filePathToRead">File path of the unsorted names</param>
        /// <returns>Array of Strings</returns>
        public string[] ReadNames(string filePathToRead)
        {
            if (filePathToRead == null) { throw new ArgumentNullException(); }
            if (!File.Exists(filePathToRead)) { throw new FileNotFoundException(); }
            return File.ReadAllLines(filePathToRead);
        }

        /// <summary>
        /// Sorts the names in the string array param, first by lastname then by firstname using LINQ
        /// </summary>
        /// <param name="namesToSort">The names to be sorted</param>
        /// <returns>IEnumerable<string/></returns>
        public IEnumerable<string> SortNames(string[] namesToSort)
        {
            if (namesToSort.Length < 1) { throw new ArgumentNullException(); }

            var sortedNames = namesToSort.Select(l => l.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).ToArray())
                .OrderBy(l => l[0])
                .ThenBy(l => l[1])
                .Select(l => string.Join(", ", l));

            return sortedNames;
        }

        /// <summary>
        /// Writes the contents of the IEnumerable<string/> param to the text file provided
        /// </summary>
        /// <param name="sortedNames">The sorted names IEnumerable<string/></param>
        /// <param name="filePathToWrite">The file path to store the contents of the sorted file</param>
        /// <returns>String contents of the written file</returns>
        public string WriteNames(IEnumerable<string> sortedNames, string filePathToWrite)
        {
            if (filePathToWrite == null) { throw new ArgumentNullException(); }
            File.WriteAllLines(filePathToWrite, sortedNames);
            return File.ReadAllText(filePathToWrite);
        }
    }
}
