﻿using System;

namespace NameSorter
{
    /// <summary>
    /// This factory is responsible for providing the different sorting classes
    /// required by the Program's constructor during Dependency Injection
    /// Every single sorting method should return an implementation of INameSorter 
    /// </summary>
    public class NameSorterFactory
    {
        public INameSorter CreateLastNameFirstNameSorter()
        {
            return new LastNameFirstNameSorter();
        }

        public INameSorter CreateFirstNameLastNameSorter()
        {
            throw new NotImplementedException();
        }

        public INameSorter CreateOnlyLastNameSorter()
        {
            throw new NotImplementedException();
        }

        public INameSorter CreateOnlyFirstNameSorter()
        {
            throw new NotImplementedException();
        }
    }
}
