﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace NameSorter
{
    /// <summary>
    /// The Program execution starts within this Bootstrap class
    /// Usage :- "Usage : NameSorter.exe <file-to-be-sorted.txt/>"
    /// </summary>
    public class Bootstrap
    { 
        public static int Main(string[] args)
        {
#if DEBUG
            args = new[] { @"C:\Users\Reshma\Documents\full-names.txt" };
#endif
            // Only one argument is allowed as the paramter when calling NameSorter 
            if (args.Length != 1 || !File.Exists(args[0]))
            {
                Console.WriteLine("Usage : NameSorter.exe <file-to-be-sorted.txt>");
                Console.WriteLine("Please enter a valid filepath containing names to be sorted, as the first argument to the program! Only one argument is accepted!");
                PromptUserInput();
                return 1;
            }
            try {
                var nameSorterFactory = new NameSorterFactory();
                var nameSorter = nameSorterFactory.CreateLastNameFirstNameSorter();
                var program = new Program(args[0], nameSorter);
                program.Sort();
            }
            catch (Exception e)
            {
                Console.WriteLine("Sorry but an error occurred while bootstrapping the console application! Please try again!");
                Console.WriteLine(e.Message);
                PromptUserInput();
            }
            return 0;
        }

        public static void PromptUserInput()
        {
            Console.WriteLine("Press any key to continue -> _ ");
            Console.ReadKey();
        }
    }

    /// <summary>
    /// The main Program that connects various classes and triggers the program flow
    /// </summary>
    public class Program
    {
        protected string FilePathToRead { get; }
        protected INameSorter NameSorter { get; }
        protected string FilePathToWrite { get; }

        /// <summary>
        /// Program Constructor
        /// </summary>
        /// <param name="filePathToRead">The location of the unsorted file</param>
        /// <param name="nameSorter">Dependency inject any class that implements the INameSorter interface</param>
        public Program(string filePathToRead, INameSorter nameSorter)
        {
            FilePathToRead = filePathToRead;
            NameSorter = nameSorter;
            var fileExt = Path.GetExtension(FilePathToRead);
            var fileName = Path.GetFileNameWithoutExtension(FilePathToRead);
            var directoryPath = Path.GetDirectoryName(FilePathToRead);
            var fileNameSuffix = "-to-" + ConvertPascalToHyphenCase(NameSorter.GetType().Name);

            if (directoryPath != null)
            {
                FilePathToWrite = Path.Combine(directoryPath, fileName + fileNameSuffix + fileExt);
            }
        }

        /// <summary>
        /// This sort method is responsible for calling, all the required class methods to perform sorting
        /// </summary>
        public void Sort()
        {
            try {
                var namesToSort = NameSorter.ReadNames(FilePathToRead);
                var sortedNames = NameSorter.SortNames(namesToSort);
                var fileContents = NameSorter.WriteNames(sortedNames, FilePathToWrite);
                Console.WriteLine();
                Console.WriteLine("### *********** Start - File Contents ************ ###");
                Console.WriteLine();
                Console.WriteLine(fileContents);
                Console.WriteLine("### *********** End - File Contents ************ ###");
                Console.WriteLine();
                Console.WriteLine("Finished : Sorted names and saved in file -> \"{0}\"", FilePathToWrite);
                Bootstrap.PromptUserInput();
            }
            catch (Exception e) {
                Console.WriteLine("Sorry but an error occurred while running the names sorting process! Please try again!");
                Console.WriteLine(e.Message);
                Bootstrap.PromptUserInput();
            }            
        }

        /// <summary>
        /// Converts a PascalCaseString to a hyphen-seperated-string
        /// </summary>
        /// <param name="pascalCaseString">Pascal string version</param>
        /// <returns>hyphenated string version</returns>
        public static string ConvertPascalToHyphenCase(string pascalCaseString)
        {
            return string.IsNullOrEmpty(pascalCaseString) ? null 
                : Regex.Replace(pascalCaseString, @"([a-z])([A-Z])", "$1-$2").ToLower();
        }
    }    
}
