﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NameSorter;

namespace NameSorterTests
{
    [TestClass()]
    public class LastNameFirstNameSorterTests
    {
        protected static LastNameFirstNameSorter LastNameFirstNameSorter;

        [ClassInitialize()]
        public static void InitialClassSetup(TestContext context)
        {
            LastNameFirstNameSorter = new LastNameFirstNameSorter();
        }

        [ClassCleanup()]
        public static void FinalClassCleanup()
        {
            LastNameFirstNameSorter = null;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ReadNamesTest_FilePathToReadIsNull_Throws()
        {
            // Arrange
            string filePathToRead = null;
            // Act
            LastNameFirstNameSorter.ReadNames(filePathToRead);
            // Assert is handled by the ExpectedException == ArgumentNullException
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SortNamesTest_NamesToSortArrayIsNull_Throws()
        {
            // Arrange
           var namesToSort = new string[0];
            // Act
            LastNameFirstNameSorter.SortNames(namesToSort);
            // Assert is handled by the ExpectedException == ArgumentNullException
        }

        [TestMethod()]
        public void SortNamesTest_ReturnsNamesSortedLastNameThenFirstName_Success()
        {
            // Arrange
            string[] namesToSort = {
                "Venugopal, Reshma",
                "Kalyan, Eduth",
                "Adams, John",
                "Doe, John",
                "Smith, David",
                "Adams, Bryan",
                "Venugopal, Chandrika",
                "Diaz, Cameron",
                "Tendulkar, Sachin",
                "Ganguly, Saurav",
                "Tendulkar, Rahul",
                "Warne, Shane",
                "Donald, Allan",
                "Wilkinson, Tom",
                "Morrison, Bruce",
                "Antony, Sharan",
                "Matthew, Alex",
                "Martin, Ricky",
                "Richards, Cliff",
                "Adams, Bryan",
                "Fletcher, Margaret"
            };

            IEnumerable<string> expectedSortedNames = new [] {
                "Adams, Bryan",
                "Adams, Bryan",
                "Adams, John",
                "Antony, Sharan",
                "Diaz, Cameron",
                "Doe, John",
                "Donald, Allan",
                "Fletcher, Margaret",
                "Ganguly, Saurav",
                "Kalyan, Eduth",
                "Martin, Ricky",
                "Matthew, Alex",
                "Morrison, Bruce",
                "Richards, Cliff",
                "Smith, David",
                "Tendulkar, Rahul",
                "Tendulkar, Sachin",
                "Venugopal, Chandrika",
                "Venugopal, Reshma",
                "Warne, Shane",
                "Wilkinson, Tom"
            };

            // Act
            var sortedNames = LastNameFirstNameSorter.SortNames(namesToSort);
            // Assert
            Assert.IsTrue(expectedSortedNames.SequenceEqual(sortedNames));
        }

        [TestMethod()]
        public void SortNamesTest_FailureScenario()
        {
            // Arrange
            string[] namesToSort = {
                "Kalyan, Eduth",
                "Adams, John",
                "Doe, John",
                "Smith, David",
                "Warne, Shane",
                "Donald, Allan",
                "Wilkinson, Tom",
                "Morrison, Bruce",
                "Antony, Sharan",
                "Matthew, Alex",
                "Martin, Ricky",
                "Richards, Cliff",
                "Adams, Bryan",
                "Fletcher, Margaret"
            };

            IEnumerable<string> expectedSortedNames = new[] {
                "Kalyan, Eduth",
                "Adams, John",
                "Doe, John",
                "Smith, David",
                "Warne, Shane",
                "Donald, Allan",
                "Wilkinson, Tom",
                "Morrison, Bruce",
                "Antony, Sharan",
                "Matthew, Alex",
                "Martin, Ricky",
                "Richards, Cliff",
                "Adams, Bryan",
                "Fletcher, Margaret"
            };

            // Act
            var sortedNames = LastNameFirstNameSorter.SortNames(namesToSort);
            // Assert
            Assert.IsFalse(expectedSortedNames.SequenceEqual(sortedNames));
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WriteNamesTest_FilePathToWriteIsNull_Throws()
        {
            // Arrange
            IEnumerable<string> sortedNames = new string[0];
            string filePathToWrite = null;
            // Act
            LastNameFirstNameSorter.WriteNames(sortedNames, filePathToWrite);
            // Assert is handled by the ExpectedException == ArgumentNullException
        }
    }
}